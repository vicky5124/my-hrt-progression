# My HRT Progression Website

This is the source code of [my hrt progression](https://hrt.vicky.rs/hrt/)!

## TODO

- [X] Remove plain text tokens.
- [X] Display comments on load.
- [ ] Make the form draggable.
- [ ] Allow to hide the form.
- [ ] Make sure it works and looks good on mobile.
- [X] Implement submit rate limits.
- [X] Make a static mirror.
- [X] Don't show comments by default.
- [X] Fix Firefox Mobile smaller heading sizes.
- [ ] Implement comment replies.
- [ ] Implement comment edits.
- [ ] Fix tracking protection from reddit request.

## Running this for yourself

You will need:

- A POSIX OS like Linux, *BSD or MacOS.
- Rust Nightly
- wasm-bindgen
- wasm-opt

Configure the project:

- Add "hrt.md" to `/static`
- Update `REDDIT_OAUTH_URL` from `/build.sh` to match your reddit application.
- Create `/web_server/.env` and provide the following values:

```bash
DATABASE_URL="postgres://username:password@host:port/database_name"
# https://www.reddit.com/prefs/apps
REDDIT_APPLICATION_ID=""
REDDIT_SECRET=""
REDDIT_REDIRECT_URL=""
# for --release builds of the server, use _RELEASE for thes env vars
```

Running the project:

- Run `./build.sh && cd web_server && cargo run`
- Open `http://localhost:4000/hrt` in a web browser.
