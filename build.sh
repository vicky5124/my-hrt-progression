#!/bin/sh


project_name='hrt'

if [ "$1" = "--release" ]; then
    (
        set -x

        cp static/index.html static_release/
        cp static/*.css static_release/
        cp static/link.svg static_release/
    )

    export REDDIT_OAUTH_URL="https://www.reddit.com/api/v1/authorize.compact?client_id=vlkYUmVNWwkxK1CjJ6dLsA&response_type=code&state=RANDOM_STRING&redirect_uri=https%3A%2F%2Fhrt.vicky.rs%2Fhrt%2Flogin&duration=temporary&scope=identity"

    path="static_release"
else
    export REDDIT_OAUTH_URL="https://www.reddit.com/api/v1/authorize.compact?client_id=5y3oafzJsfd7H-T024-vsw&response_type=code&state=RANDOM_STRING&redirect_uri=http%3A%2F%2F192.168.1.2%3A4000%2Fhrt%2Flogin&duration=temporary&scope=identity"

    path="static"
fi

(
    set -x
    
    cargo build --release --target wasm32-unknown-unknown || exit
    wasm-bindgen "target/wasm32-unknown-unknown/release/${project_name}.wasm" --out-dir ${path} --no-modules --no-typescript || exit
    wasm-opt ${path}/${project_name}_bg.wasm -Oz --fast-math -o ${path}/${project_name}_bg.wasm || exit
)

#echo "Running server..."
#basic-http-server static -a "192.168.1.2:4000"
