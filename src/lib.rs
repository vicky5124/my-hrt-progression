#[macro_use]
extern crate tracing;

pub mod error;
pub mod markdown;

use std::collections::HashMap;
use std::fmt;
use std::future::Future;
use std::ops::Not;

use itertools::Itertools;
use serde::Deserialize;
use wasm_bindgen::JsCast;
use yew::prelude::*;

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

type QuadRequestData = (
    String,
    Option<SessionData>,
    Vec<Comment>,
    HashMap<String, RedditUser>,
);
type BoxFuture<T> = std::pin::Pin<Box<dyn Future<Output = T>>>;

#[derive(Deserialize, Debug, Clone)]
pub struct SessionData {
    pub id: String,
    pub name: String,
    pub access_token: String,
}

#[derive(Deserialize, Debug)]
pub struct Comment {
    pub id: i32,
    pub heading_id: String,
    pub username: String,
    pub comment: String,
    pub date: chrono::NaiveDateTime,
    //pub date: String,
}

#[derive(Deserialize, Debug)]
pub struct RedditUser {
    pub data: RedditUserData,
}

#[derive(Deserialize, Debug)]
pub struct RedditUserData {
    pub icon_img: String,
}

#[derive(PartialEq, Clone, Copy)]
pub enum Theme {
    Dark,
    Light,
}

impl Not for Theme {
    type Output = Self;

    fn not(self) -> Self::Output {
        match self {
            Self::Dark => Self::Light,
            Self::Light => Self::Dark,
        }
    }
}

impl fmt::Display for Theme {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Dark => write!(f, "markdown-body-dark"),
            Self::Light => write!(f, "markdown-body"),
        }
    }
}

/// The possible states a fetch request can be in.
pub enum FetchState {
    Done,
    Fetching,
    Success(QuadRequestData),
    Failed(reqwasm::Error),
}

/// Fetches markdown from Yew's README.md.
///
/// Consult the following for an example of the fetch api by the team behind web_sys:
/// https://rustwasm.github.io/wasm-bindgen/examples/fetch.html
async fn fetch_markdown(url: &'static str) -> Result<QuadRequestData, reqwasm::Error> {
    let resp_md = reqwasm::http::Request::get(url).send().await?;

    if resp_md.status() == 404 {
        Ok((
            "Original markdown file could not be found, please, go to the mirror [here](/hrt-static)"
                .to_string(),
            None,
            vec![],
            HashMap::new(),
        ))
    } else {
        let (all_comments, resp_cookie) = futures::join!(
            reqwasm::http::Request::get("/hrt/comments").send(),
            reqwasm::http::Request::get("/hrt/decode_cookie").send(),
        );

        let all_comments = all_comments?.json::<Vec<Comment>>().await?;

        let pfps: HashMap<String, RedditUser> = futures::future::join_all({
            all_comments
                .iter()
                .map(|i| i.username.to_string())
                .dedup()
                .map(|i| -> BoxFuture<Option<(String, RedditUser)>> {
                    Box::pin(async move {
                        let url = format!("https://www.reddit.com/user/{}/about.json", i);

                        Some((
                            i.to_string(),
                            reqwasm::http::Request::get(&url)
                                .send()
                                .await
                                .ok()?
                                .json::<RedditUser>()
                                .await
                                .ok()?,
                        ))
                    })
                })
        })
        .await
        .into_iter()
        .flatten()
        .collect();

        let resp_cookie = resp_cookie?;
        let mut cookie_data = None;

        if resp_cookie.status() != 404 {
            cookie_data = Some(resp_cookie.json::<SessionData>().await?);
        }

        Ok((resp_md.text().await?, cookie_data, all_comments, pfps))
    }
}

pub enum Msg {
    SetMarkdownFetchState(FetchState),
    SetTheme(Theme),
    ClickedHeading(MouseEvent),
    ContextMenuHeading(MouseEvent),
    ToggleComments,
}

pub struct Model {
    pub link: yew::html::Scope<Self>,
    pub theme: Theme,
    pub markdown: FetchState,
    pub has_jumped: bool,
    pub comments_enabled: bool,
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        debug!("create");

        ctx.link().send_future(async {
            match fetch_markdown("/hrt/hrt.md").await {
                Ok(md) => Msg::SetMarkdownFetchState(FetchState::Success(md)),
                Err(err) => Msg::SetMarkdownFetchState(FetchState::Failed(err)),
            }
        });
        ctx.link()
            .send_message(Msg::SetMarkdownFetchState(FetchState::Fetching));

        let mut dark = false;

        let window = gloo_utils::window();
        if let Ok(Some(media)) = window.match_media("(prefers-color-scheme: dark)") {
            dark = media.matches();
        }

        let theme = if dark { Theme::Dark } else { Theme::Light };

        Self {
            link: ctx.link().clone(),
            theme,
            markdown: FetchState::Fetching,
            has_jumped: false,
            comments_enabled: false,
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        debug!("update");

        match msg {
            Msg::SetMarkdownFetchState(fetch_state) => {
                self.markdown = fetch_state;
                true
            }
            Msg::SetTheme(theme) => {
                self.theme = theme;

                let not_theme = theme.not().to_string();

                if let Some(element) = gloo_utils::document()
                    .get_elements_by_class_name(&not_theme)
                    .item(0)
                {
                    let res = element.set_attribute("class", &theme.to_string());
                    if let Err(why) = res {
                        error!("{:?}", why);
                    }
                }

                let body_element = gloo_utils::document().body().unwrap();

                let res = match theme {
                    Theme::Dark => body_element.set_attribute("class", "body-dark"),
                    Theme::Light => body_element.set_attribute("class", "body"),
                };

                if let Err(why) = res {
                    error!("{:?}", why);
                }

                true
            }
            Msg::ClickedHeading(event) | Msg::ContextMenuHeading(event) => {
                event.prevent_default();

                let target = event.target().unwrap();
                let element = target.dyn_ref::<web_sys::HtmlHeadingElement>().unwrap();

                let id = element.get_attribute("id").unwrap();

                if event.button() == 2 {
                    if let Some(form_element) =
                        gloo_utils::document().get_element_by_id("submit_form")
                    {
                        let offset_top = element.offset_top();

                        let html_element = form_element.dyn_ref::<web_sys::HtmlElement>().unwrap();
                        html_element.set_hidden(false);

                        let style = html_element.style();
                        let _ = style.set_property("margin-top", &format!("{}px", offset_top + 5));

                        let input = html_element
                            .children()
                            .item(0)
                            .unwrap()
                            .children()
                            .item(0)
                            .unwrap();

                        let html_input = input.dyn_ref::<web_sys::HtmlInputElement>().unwrap();
                        let _ = html_input.set_value(&id);

                        let title = html_element.children().item(1).unwrap();

                        let _ = title.set_inner_html(&element.inner_text());
                    }
                } else if event.button() == 0 {
                    if let Err(why) = gloo_utils::window().location().set_hash(&id) {
                        error!("{:#?}", why);
                    }

                    if !event.shift_key() {
                        element.scroll_into_view();
                    }
                }

                //info!("{:#?}", element.inner_text());

                false
            }
            Msg::ToggleComments => {
                self.comments_enabled = !self.comments_enabled;

                let elements = gloo_utils::document().get_elements_by_class_name("comment");

                for idx in 0..elements.length() {
                    let element = elements.item(idx).unwrap();
                    let html_element = element.dyn_ref::<web_sys::HtmlElement>().unwrap();
                    html_element.set_hidden(!self.comments_enabled);
                }

                false
            }
        }
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        debug!("view");
        match &self.markdown {
            FetchState::Done => html! {},
            FetchState::Fetching => html! {
                <>
                    <p>{ "The website is loading, please be patient!" }</p>
                    <p>{ "If it doesn't, there's a semi-static version of the site available at "}<a href="/hrt-static">{ "/hrt-static" }</a></p>
                </>
            },
            FetchState::Success(data) => {
                info!("{:#?}", data.3);
                html! { markdown::render_markdown(self, &data.0, &data.1, &data.2, &data.3) }
            }
            FetchState::Failed(err) => {
                html! {
                    <>
                        <b>{ err }</b>
                        <p>{ "There's a semi-static version of the site available at "}<a href="/hrt-static">{ "/hrt-static" }</a></p>
                    </>
            }
            }
        }
    }

    fn rendered(&mut self, _ctx: &Context<Self>, first_render: bool) {
        debug!("rendered; first: {}", first_render);

        if !self.has_jumped {
            if let FetchState::Success(_) = self.markdown {
                let hash_fragment: String = gloo_utils::window()
                    .location()
                    .hash()
                    .unwrap()
                    .chars()
                    .skip(1)
                    .collect();

                if !hash_fragment.is_empty() {
                    if let Some(element) = gloo_utils::document().get_element_by_id(&hash_fragment)
                    {
                        element.scroll_into_view()
                    }
                }

                self.has_jumped = true;
            }
        }
    }
}

#[wasm_bindgen::prelude::wasm_bindgen]
pub fn start() {
    tracing_wasm::set_as_global_default();
    info!("Loading up!");
    yew::start_app::<Model>();
}
