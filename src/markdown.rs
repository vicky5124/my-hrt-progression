/// Original author of this code is [Nathan Ringo](https://github.com/remexre)
/// Source: https://github.com/acmumn/mentoring/blob/master/web-client/src/view/markdown.rs
use pulldown_cmark::{CodeBlockKind, Event, Options, Parser, Tag};
use yew::prelude::*;
use yew::virtual_dom::{VNode, VTag, VText};

/// Renders a string of Markdown to HTML with the default options (footnotes
/// disabled, tables enabled).
pub fn render_markdown(
    model: &crate::Model,
    src: &str,
    user_data: &Option<crate::SessionData>,
    comments: &[crate::Comment],
    pfps: &std::collections::HashMap<String, crate::RedditUser>,
) -> Html {
    let mut elems = vec![];
    let mut spine = vec![];

    macro_rules! add_child {
        ($child:expr) => {{
            let l = spine.len();
            assert_ne!(l, 0);
            spine[l - 1].add_child($child);
        }};
    }

    let mut options = Options::empty();
    options.insert(Options::ENABLE_TABLES);

    for ev in Parser::new_ext(src, options) {
        match ev {
            Event::Start(tag) => {
                spine.push(make_tag(tag));
            }
            Event::End(tag) => {
                // TODO Verify stack end.
                let l = spine.len();
                assert!(l >= 1);
                let mut top = spine.pop().unwrap();

                let mut id = String::new();

                let children_clone = top.children().clone();

                if let Tag::Heading(..) = tag {
                    let mut clear_idx = 2;

                    for (idx, c) in top.children_mut().unwrap().iter_mut().enumerate() {
                        if let VNode::VText(vtext) = c {
                            if idx == clear_idx {
                                vtext.text = "".into();
                            }
                        }

                        if let VNode::VTag(vtag) = c {
                            if vtag.tag().starts_with('h') {
                                if let VNode::VText(vtext) = children_clone.get(idx + 1).unwrap() {
                                    clear_idx = idx + 1;

                                    let text: String = {
                                        vtext
                                            .text
                                            .chars()
                                            .filter_map(|mut c| {
                                                c = c.to_ascii_lowercase();
                                                if c == ' ' {
                                                    Some('-')
                                                } else if c.is_ascii_alphanumeric() {
                                                    Some(c)
                                                } else {
                                                    None
                                                }
                                            })
                                            .collect()
                                    };
                                    vtag.add_attribute("id", text.clone());
                                    vtag.add_child(VNode::VText(vtext.clone()));
                                    id = text;
                                }

                                let local_click_cb = model.link.callback(|event: MouseEvent| {
                                    crate::Msg::ClickedHeading(event)
                                });

                                let local_context_menu_cb =
                                    model.link.callback(|event: MouseEvent| {
                                        crate::Msg::ContextMenuHeading(event)
                                    });

                                let listener_click =
                                    yew::html::onmousedown::Wrapper::new(local_click_cb);

                                let listener_context_menu =
                                    yew::html::oncontextmenu::Wrapper::new(local_context_menu_cb);

                                vtag.set_listener(Box::new([
                                    Some(std::rc::Rc::new(listener_click)),
                                    Some(std::rc::Rc::new(listener_context_menu)),
                                ]));
                            }
                        }
                    }
                }

                /*
                <div class="comment">
                    <div class="outer_container" height="48">
                        <img src="https://loremflickr.com/48/48" width="48" height="48" alt="img"/>

                        <div class="inner_container">
                            <p>u/vicky5124 · 16 Dec 2021 20:51:05</p>
                            <button>Reply to this comment</button>
                        </div>
                    </div>

                    <p>short comment.</p>
                </div>
                */

                let mut html_comments = vec![];

                for comment in comments.iter().filter(|i| i.heading_id == id) {
                    let html_comment = html! {
                        <div class="comment" hidden=true>
                            <div class="outer_container" height="48">
                                <img src={
                                    pfps
                                        .get(&comment.username)
                                        .map(|i| i
                                            .data
                                            .icon_img
                                            .split('?')
                                            .next()
                                            .unwrap()
                                            .to_string()
                                        )
                                        .unwrap_or_else(|| "https://i.imgur.com/ws2kAA0.png".to_string())
                                } width="48" height="48" alt="img"/>

                                <div class="inner_container">
                                    <p>{ format!("u/{} · {}", comment.username, comment.date.format("On: %d %b %Y %H:%M:%S UTC")) }</p>
                                    <button>{ "Reply to this comment (disabled)" }</button>
                                </div>
                            </div>

                            <p>{ &comment.comment }</p>
                        </div>
                    };

                    html_comments.push(html_comment);
                }

                if l == 1 {
                    elems.push(top);

                    for html_comment in html_comments {
                        if let VNode::VTag(comment) = html_comment {
                            elems.push(*comment);
                        }
                    }
                } else {
                    spine[l - 2].add_child(top.into());
                }
            }
            Event::Text(text) => add_child!(VText::new(text.to_string()).into()),
            Event::Rule => add_child!(VTag::new("hr").into()),
            Event::SoftBreak => add_child!(VText::new("\n").into()),
            Event::HardBreak => add_child!(VTag::new("br").into()),
            _ => warn!("Unknown event: {:#?}", ev),
        }
    }

    if elems.len() == 1 {
        VNode::VTag(Box::new(elems.pop().unwrap()))
    } else {
        let mut body_class = "markdown-body";

        if let crate::Theme::Dark = model.theme {
            body_class = "markdown-body-dark";

            let body_element = gloo_utils::document().body().unwrap();

            let res = body_element.set_attribute("class", "body-dark");

            if let Err(why) = res {
                error!("{:?}", why);
            }
        } else {
            let body_element = gloo_utils::document().body().unwrap();

            let res = body_element.set_attribute("class", "body");

            if let Err(why) = res {
                error!("{:?}", why);
            }
        }

        html! {
            <>
                <button onclick={model.link.callback(|_| crate::Msg::SetTheme(crate::Theme::Dark))}>
                    { "🌑" }
                </button>
                <button onclick={model.link.callback(|_| crate::Msg::SetTheme(crate::Theme::Light))}>
                    { "☀️" }
                </button>

                if let Some(user_data) = user_data {
                    <form action="/hrt/submit" method="POST" enctype="multipart/form-data" id="submit_form" hidden=true>
                        <p hidden=true>
                            <input type="text" name="id" value="1805" class="center" />
                        </p>
                        <p id="input"></p>
                        <p>
                            <textarea name="comment" rows="5" cols="32" placeholder="Input your comment here." class="center" id="input"></textarea>
                        </p>
                        <p>
                            <button>{ "Submit" }</button>
                        </p>
                    </form>

                    <a href="/hrt/logout">
                        <button class="right">{ format!("Logout from u/{}", user_data.name) }</button>
                    </a>
                } else {
                    <a href={ env!("REDDIT_OAUTH_URL") }>
                        <button class="right">{ "Login with Reddit to comment!" }</button>
                    </a>
                }

                <button class="right" onclick={model.link.callback(|_| crate::Msg::ToggleComments)}>{ "💬 Toggle comments" }</button>

                <div class={body_class}>{ for elems.into_iter() }</div>
            </>
        }
    }
}

fn make_tag(t: Tag) -> VTag {
    match t {
        Tag::Paragraph => VTag::new("p"),
        Tag::Heading(n, ..) => {
            let mut h = VTag::new(n.to_string());
            h.add_attribute("id", "align");

            let mut img = VTag::new("img");
            img.add_attribute("src", "link.svg");
            img.add_attribute("class", "link");
            img.add_attribute("id", "align");

            let mut div = VTag::new("div");
            div.add_attribute("class", "edit_hover_class");

            div.add_child(VNode::VTag(Box::new(img)));
            div.add_child(VNode::VTag(Box::new(h)));

            div
        }
        Tag::BlockQuote => {
            let mut el = VTag::new("blockquote");
            el.add_attribute("class", "blockquote");
            el
        }
        Tag::CodeBlock(code_block_kind) => {
            let mut el = VTag::new("code");

            if let CodeBlockKind::Fenced(lang) = code_block_kind {
                // Different color schemes may be used for different code blocks,
                // but a different library (likely js based at the moment) would be necessary to actually provide the
                // highlighting support by locating the language classes and applying dom transforms
                // on their contents.
                match lang.as_ref() {
                    "html" => el.add_attribute("class", "html-language"),
                    "rust" => el.add_attribute("class", "rust-language"),
                    "java" => el.add_attribute("class", "java-language"),
                    "c" => el.add_attribute("class", "c-language"),
                    _ => {} // Add your own language highlighting support
                };
            }

            el
        }
        Tag::List(None) => VTag::new("ul"),
        Tag::List(Some(1)) => VTag::new("ol"),
        Tag::List(Some(ref start)) => {
            let mut el = VTag::new("ol");
            el.add_attribute("start", start.to_string());
            el
        }
        Tag::Item => VTag::new("li"),
        Tag::Table(_) => {
            let mut el = VTag::new("table");
            el.add_attribute("class", "table");
            el
        }
        Tag::TableHead => VTag::new("th"),
        Tag::TableRow => VTag::new("tr"),
        Tag::TableCell => VTag::new("td"),
        Tag::Emphasis => {
            let mut el = VTag::new("span");
            el.add_attribute("class", "font-italic");
            el
        }
        Tag::Strong => {
            let mut el = VTag::new("span");
            el.add_attribute("class", "font-weight-bold");
            el
        }
        Tag::Link(_link_type, ref href, ref title) => {
            let mut el = VTag::new("a");
            el.add_attribute("href", href.to_string());
            let title = title.clone().into_string();
            if !title.is_empty() {
                el.add_attribute("title", title);
            }
            el
        }
        Tag::Image(_link_type, ref src, ref title) => {
            let mut el = VTag::new("img");
            el.add_attribute("src", src.to_string());
            let title = title.clone().into_string();
            if !title.is_empty() {
                el.add_attribute("title", title);
            }
            el
        }
        Tag::FootnoteDefinition(ref _footnote_id) => VTag::new("span"), // Footnotes are not rendered as anything special
        Tag::Strikethrough => {
            let mut el = VTag::new("span");
            el.add_attribute("class", "text-decoration-strikethrough");
            el
        }
    }
}
