#![allow(unreachable_code)]
#[macro_use]
extern crate rouille;

use std::fs::File;
use std::io;

use parking_lot::Mutex;
use postgres::{Client, NoTls, Transaction};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Debug)]
struct Comment {
    id: i32,
    heading_id: String,
    username: String,
    comment: String,
    date: chrono::NaiveDateTime,
}

#[derive(Serialize, Debug, Clone)]
struct SessionData {
    id: String,
    name: String,
    access_token: String,
}

#[derive(Deserialize, Debug)]
struct RedditOauthQs {
    code: Option<String>,
    error: Option<String>,
}

#[derive(Serialize, Debug)]
struct RedditOauthSendForm {
    grant_type: String,
    code: String,
    redirect_uri: String,
}

#[derive(Deserialize, Debug)]
struct RedditOauthResponse {
    access_token: String,
}

#[derive(Deserialize, Debug)]
struct RedditOauthMe {
    id: String,
    name: String,
}

fn main() {
    dotenv::dotenv().unwrap();

    let db = {
        let db = Client::connect(
            &std::env::var(if cfg!(debug_assertions) {
                "DATABASE_URL"
            } else {
                "DATABASE_URL_RELEASE"
            })
            .unwrap(),
            NoTls,
        );
        Mutex::new(db.expect("Failed to connect to database"))
    };

    {
        let sql = "CREATE TABLE IF NOT EXISTS comment (
            id SERIAL PRIMARY KEY,
            heading_id TEXT NOT NULL,
            username TEXT DEFAULT 'Anonymous' NOT NULL,
            comment TEXT NOT NULL,
            date TIMESTAMP NOT NULL
        );";
        db.lock()
            .execute(sql, &[])
            .expect("Failed to initialize database");

        let sql = "CREATE TABLE IF NOT EXISTS rate_limit (
            id TEXT PRIMARY KEY,
            date TIMESTAMP NOT NULL
        );";
        db.lock()
            .execute(sql, &[])
            .expect("Failed to initialize database");
    }

    let ip = if cfg!(debug_assertions) {
        "0.0.0.0:4000"
    } else {
        "0.0.0.0:4444"
    };

    println!("Now listening on http://{}", ip);

    let sessions_storage: dashmap::DashMap<String, SessionData> = dashmap::DashMap::new();

    rouille::start_server(ip, move |request| {
        rouille::log(request, io::stdout(), || {
            rouille::session::session(request, "SID", 3600, |session| {
                let mut session_data = if session.client_has_sid() {
                    sessions_storage.get(session.id()).map(|data| data.clone())
                } else {
                    None
                };

                let mut db_open = db.lock();

                let mut transaction = db_open.transaction().unwrap();

                let response = routes(request, &mut transaction, &mut session_data);

                if let Some(d) = session_data {
                    sessions_storage.insert(session.id().to_owned(), d);
                } else if session.client_has_sid() {
                    sessions_storage.remove(session.id());
                }

                if response.is_success() {
                    transaction.commit().unwrap();
                }

                response
            })
        })
    });
}

fn routes(
    request: &rouille::Request,
    db: &mut Transaction,
    session_data: &mut Option<SessionData>,
) -> rouille::Response {
    if cfg!(debug_assertions) {
        std::env::set_current_dir("../static").unwrap_or_else(|_| {
            std::env::set_current_dir("./static").unwrap();
        });
    } else {
        std::env::set_current_dir("../static_release").unwrap_or_else(|_| {
            std::env::set_current_dir("./static_release").unwrap();
        });
    }

    router!(request,
        (GET) (/hrt/comments) => {
            let mut out = Vec::new();

            for row in &db.query("SELECT * FROM comment", &[]).unwrap() {
                out.push(Comment {
                    id: row.get(0),
                    heading_id: row.get(1),
                    username: row.get(2),
                    comment: row.get(3),
                    date: row.get(4),
                });
            }

            return rouille::Response::json(&out);
        },

        (GET) (/hrt/logout) => {
            *session_data = None;

            return rouille::Response::redirect_303("/hrt/");
        },

        (GET) (/hrt/login) => {
            let data: RedditOauthQs = serde_qs::from_str(request.raw_query_string()).unwrap();

            if let Some(err) = data.error {
                return rouille::Response::html(format!("<p>Oauth Error: {}</p>", err));
            }

            let code = data.code.unwrap();

            let client = reqwest::blocking::Client::builder()
                .user_agent("HRT:v0.1.0-alpha (by u/vicky5124)")
                .build()
                .unwrap();

            let request = client.post("https://www.reddit.com/api/v1/access_token")
                .form(&RedditOauthSendForm {
                    grant_type: "authorization_code".to_string(),
                    code,
                    redirect_uri: std::env::var(
                        if cfg!(debug_assertions) {
                            "REDDIT_REDIRECT_URL"
                        } else {
                            "REDDIT_REDIRECT_URL_RELEASE"
                        }
                    ).unwrap(),
                })
                .basic_auth(
                    &std::env::var(
                        if cfg!(debug_assertions) {
                            "REDDIT_APPLICATION_ID"
                        } else {
                            "REDDIT_APPLICATION_ID_RELEASE"
                        }
                    ).unwrap(),
                    Some(
                        &std::env::var(
                            if cfg!(debug_assertions) {
                                "REDDIT_SECRET"
                            } else {
                                "REDDIT_SECRET_RELEASE"
                            }
                        )
                        .unwrap()
                    ),
                )
                .send()
                .unwrap()
                .json::<RedditOauthResponse>()
                .unwrap();

            println!("{:#?}", request);

            let me = client.get("https://oauth.reddit.com/api/v1/me")
                .bearer_auth(&request.access_token)
                .send()
                .unwrap()
                .json::<RedditOauthMe>()
                .unwrap();

            println!("{:#?}", me);

            *session_data = Some(SessionData {
                id: me.id,
                name: me.name,
                access_token: request.access_token,
            });

            return rouille::Response::redirect_303("/hrt/");
        },

        _ => (),
    );

    if let Some(session_data) = session_data.as_ref() {
        router!(request,
            (POST) (/hrt/submit) => {
                let mut data = try_or_400!(post_input!(request, {
                    comment: String,
                    id: String,
                }));

                // Remove zero-width spaces
                data.comment = data.comment.chars().filter(|c| *c != '\u{200b}' && *c != '\u{17b5}').collect();
                data.comment = data.comment.split_whitespace().collect();

                if data.comment.len() < 32 {
                    return rouille::Response::html(format!(r#"<p>The minimum length for a comment is 32 characters.<br/><p>Here's your text, copy it to not lose it:</p></p><br/><tt>{}</tt><br/><p><a href="/hrt/">Go back</a>.</p>"#, data.comment));
                } else if data.comment.len() > 4000 {
                    return rouille::Response::html(format!(r#"<p>The maximum length for a comment is 4000 characters.<br/><p>Here's your text, copy it to not lose it:</p></p><br/><tt>{}</tt><br/><p><a href="/hrt/">Go back</a>.</p>"#, data.comment));
                }

                println!("Received data: {:?}", data);

                let rate_limt_opt = db.query_opt("SELECT * FROM rate_limit WHERE date > current_timestamp AND id = $1", &[&session_data.id]).unwrap();

                if let Some(rate_limt) = rate_limt_opt {
                    let date: chrono::NaiveDateTime = rate_limt.get(1);
                    let date_seconds = date.timestamp();
                    let now_seconds = chrono::Utc::now().timestamp();

                    let seconds_left = date_seconds - now_seconds;

                    return rouille::Response::html(format!(r#"<p>You can't post a comment until {} seconds have passed.<br/><p>Here's your text, copy it to not lose it:</p></p><br/><tt>{}</tt><br/><p><a href="/hrt/">Go back</a>.</p>"#, seconds_left, data.comment));
                }

                db.execute(
                    "INSERT INTO comment (heading_id, username, comment, date) VALUES ($1, $2, $3, current_timestamp)",
                    &[&data.id, &session_data.name, &data.comment],
                ).unwrap();

                db.execute(
                    "INSERT INTO rate_limit (id, date) VALUES ($1, current_timestamp + interval '2 minute') ON CONFLICT (id) DO UPDATE SET date = current_timestamp + interval '2 minute'",
                    &[&session_data.id],
                ).unwrap();

                return rouille::Response::redirect_303("/hrt/".to_string());
            },

            (GET) (/hrt/decode_cookie) => {
                return rouille::Response::json(session_data);
            },

            _ => (),
        );
    } else {
        router!(request,
            (POST) (/hrt/submit) => {
                return rouille::Response::html(r#"You need to login with Reddit first before submitting a comment. <a href="/hrt/">Go back</a>."#);
            },

            _ => (),
        );
    }

    router!(request,
        (GET) (/hrt) => {
            return rouille::Response::redirect_302("/hrt/".to_string());
        },
        (GET) (/hrt/) => {
            let file = File::open("./index.html").unwrap();

            return rouille::Response::from_file("text/html", file);
        },

        (GET) (/hrt/{path: String}) => {
            if path.is_empty() {
                return rouille::Response::redirect_303("/hrt/".to_string());
            }

            let file = try_or_404!(File::open(format!("./{}", path)));

            let mime = rouille::extension_to_mime(path.split('.').last().unwrap());

            return rouille::Response::from_file(mime, file);
        },

        _ => rouille::Response::empty_404()
    )
}
